<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="model.User" %>
<%
//セッションスコープからユーザー情報を取得
User loginUser = (User) session.getAttribute("loginUser");
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>どこつぶ</title>
<link rel="stylesheet" type="text/css" href="css/docoTsubu.css">
</head>
<body>
<div class="common_layout login_result">
	<h1>どこつぶログイン</h1>
	<% if(loginUser != null) { %>
		<p>ログインに成功しました<br>
		ようこそ<%= loginUser.getName() %>さん</p>
		<p><a href="/docoTsubu/Main">つぶやき投稿・閲覧へ</a></p>
	<% } else { %>
		<p>ログインに失敗しました</p>
		<a href="/docoTsubu/">TOPへ</a>
	<% } %>
</div>
</body>
</html>